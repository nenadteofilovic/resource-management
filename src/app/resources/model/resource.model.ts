import { Vacation } from "./vacation.model";

export class Resource{
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public expertiseLevel: string,
    public email: string,
    public vacation: Vacation[],
    public imgUrl: string,
    public backendId: string){}

    getFullName(){
      return this.firstName + ' ' + this.lastName;
    }
}
