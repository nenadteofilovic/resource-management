export class Vacation{
  constructor(
    public year: number,
    public daysPerYearAvailable: number,
    public usedDays: number,
    public daysLeft: number){}
}
