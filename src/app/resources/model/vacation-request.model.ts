import { VacationRequestStatus } from "src/app/shared/vacation.service";

export class VacationRequest{
  constructor(
    public resourceId: number,
    public from: Date,
    public to: Date,
    public numOfDays: number,
    public fixedPeriod: boolean,
    public status: VacationRequestStatus,
    public backendId?: string,
    public resourceFullName?: string){}
}
