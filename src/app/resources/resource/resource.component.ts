import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { ResourceService } from '../resource.service';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.css']
})
export class ResourceComponent implements OnInit {
  resourceForm: FormGroup = new FormGroup({});
  resourceId: number;
  editMode: boolean = false;
  editResourceChangedSub: Subscription;
  @ViewChild('editConfirm', { read: TemplateRef }) editConfirmTemplate:TemplateRef<any>;
  modalRef: BsModalRef;

  constructor(private router: Router,
              private resourceService: ResourceService,
              private route: ActivatedRoute,
              private modalService: BsModalService) { }

  ngOnInit(): void {

    this.route.params.subscribe(
      (params: Params) => {
        this.resourceId = +params['id'];
        this.editMode = params['id'] != null;
      }
    );
    //Subscribe to this Observable and catch event when secondary route is changed
    this.editResourceChangedSub = this.resourceService.editResourceChangedSubject.subscribe(
      (index: number) => {
        //Workaround: Fetch the new index of the resource (from secondary route), reload the route and navigate again to new secondary route.
        //This will trigger, reload of the component.
        this.resourceId = index;
        this.navigateAfterFormIsClosed();
        setTimeout(() => {
          let newUrl = '';
          if(this.router.url.indexOf('prj:projects') != -1){
            newUrl = this.router.url.replace('(','(res:resources/'+ this.resourceId +'//');
          }else{
            newUrl = '/manage/(res:resources/'+ this.resourceId +')';
          }
          this.router.navigateByUrl(newUrl);

        },100);
      }
    );

    this.initForm();
  }

  initForm(){
    if(this.editMode){
      let resource = this.resourceService.getResource(this.resourceId);

      this.resourceForm.addControl('firstName', new FormControl(resource.firstName, Validators.required));
      this.resourceForm.addControl('lastName', new FormControl(resource.lastName, Validators.required));
      this.resourceForm.addControl('expertiseLevel', new FormControl(resource.expertiseLevel, Validators.required));
      this.resourceForm.addControl('email', new FormControl(resource.email, [Validators.required, Validators.email]));
    }else{
      this.resourceForm.addControl('firstName', new FormControl(null, Validators.required));
      this.resourceForm.addControl('lastName', new FormControl(null, Validators.required));
      this.resourceForm.addControl('expertiseLevel', new FormControl(null, Validators.required));
      this.resourceForm.addControl('email', new FormControl(null, [Validators.required, Validators.email]));
    }

  }

  onCancel(){
    this.navigateAfterFormIsClosed();
  }

  onSave(){
    if(this.editMode){
      this.resourceService.editResource(this.resourceId, this.resourceForm.value);
    }else{
      this.resourceService.addResource(this.resourceForm.value);
    }

    this.navigateAfterFormIsClosed();

    this.modalRef = this.modalService.show(this.editConfirmTemplate, {class: 'modal-sm'});
    setTimeout(() => {
      this.modalRef.hide();
    },2000);
  }

  navigateAfterFormIsClosed(){
    let newUrl = '';
    if(this.router.url.indexOf('prj:projects') != -1){
      let delimiterIndex = this.router.url.indexOf('//');
      newUrl = this.router.url.substring(delimiterIndex + 2, this.router.url.length);
      newUrl = 'manage/(' + newUrl;
    }else{
      newUrl = '/manage';
    }
    this.router.navigateByUrl(newUrl);
  }
}
