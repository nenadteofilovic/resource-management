import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { Resource } from '../model/resource.model';
import { ResourceService } from '../resource.service';

@Component({
  selector: 'app-resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.css']
})
export class ResourceListComponent implements OnInit, OnDestroy {

  resources: Resource[] = [];
  resourcesChangedSub: Subscription = new Subscription();
  modalRef: BsModalRef;
  resourceToDelete: Resource;
  resourceIndexToDelete: number;

  constructor(private resourceService: ResourceService,
              private router: Router,
              private modalService: BsModalService) { }


  ngOnInit(): void {
    this.resourcesChangedSub = this.resourceService.resourcesChangedSubject.subscribe(
      (resources: Resource[]) => {
        this.resources = resources;
      }
    );
  }

  onNewResource(){
    let newUrl = '';
    if(this.router.url.indexOf('prj:projects') != -1){
      let delimiterIndex = this.router.url.indexOf('//');
      if(delimiterIndex == -1){
        newUrl = this.router.url.replace('(', '(res:resources//');
      }else{
        newUrl = this.router.url.substring(delimiterIndex + 2, this.router.url.length);
        newUrl = '/manage/(res:resources//' + newUrl;
      }
    }else{
      newUrl = '/manage/(res:resources)';
    }
    this.router.navigateByUrl(newUrl);
  }

  onDelete(index: number, template: TemplateRef<any>){
    this.resourceToDelete = this.resources[index];
    this.resourceIndexToDelete = index;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});

  }

  onEdit(index: number){
    //Since secondary route change does not trigger component reload, we have to notify component using Subject in the service
    let newUrl = '';
    if(this.router.url.indexOf('prj:projects') != -1){
      newUrl = this.router.url.replace('(','(res:resources/'+index+'//');
    }else{
      newUrl = '/manage/(res:resources/'+ index +')';
    }
    this.router.navigateByUrl(newUrl);
    this.resourceService.editResourceWithIndex(index);
  }

  confirm(): void {
    this.modalRef.hide();
    this.resourceService.deleteResource(this.resourceIndexToDelete);
  }

  decline(): void {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    this.resourcesChangedSub.unsubscribe();
  }

}
