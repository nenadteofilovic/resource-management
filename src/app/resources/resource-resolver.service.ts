import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { Resource } from "./model/resource.model";
import { ResourceService } from "./resource.service";

@Injectable({
  providedIn: 'root'
})
export class ResourceResolverService implements Resolve<Resource[]>{

  constructor(private resourceService: ResourceService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Resource[] | Observable<Resource[]> | Promise<Resource[]> {
    return this.resourceService.getResources();
  }

}
