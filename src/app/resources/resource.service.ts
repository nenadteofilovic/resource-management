import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { map, tap } from 'rxjs/operators';
import { Resource } from "./model/resource.model";

@Injectable({
  providedIn: 'root'
})
export class ResourceService{
  // resources: Resource[] = [
  //   new Resource(0, 'Pascal', 'Langenstein', 'Senior Software Architect', 'pascal.langenstein@bsgroup.ch', [
  //     new Vacation(2021, 12, 12, 0),
  //     new Vacation(2022, 25, 5, 20)
  //   ],
  //   'https://basaschools.co.za/wp-content/uploads/2021/04/boy-avator.png', '123abc'),
  //   new Resource(1, 'Nenad', 'Teofilovic', 'Senior Software Engineer', 'nenad.teofilovic@bsgroup.ch', [
  //     new Vacation(2021, 4, 4, 0),
  //     new Vacation(2022, 25, 0, 25)
  //   ],
  //   'http://bootdey.com/img/Content/avatar/avatar1.png', '345bnm')
  // ];
  resources: Resource[] = [];
  resourcesChangedSubject = new BehaviorSubject<Resource[]>([]);
  editResourceChangedSubject = new Subject<number>();

  constructor(private http: HttpClient){}

  getResources(){
    return this.http.get<any>('https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/resources.json')
    .pipe(
      tap(
        (response) => {
          this.resources = [];
          if(response){
            Object.entries(response).forEach(
              (entry: any) => {
                this.resources.push({...entry[1], backendId:entry[0]});
              }
            );
          }
          this.resourcesChangedSubject.next(this.resources.slice());
        }
      )
    )
  }

  getResource(index: number){
    return this.resources[index];
  }

  getResourceWithMail(email: string){
    return this.resources.find(resource => {
      return resource.email === email;
    });
  }

  getCurrentUser(): Resource{
    //TODO: Implement based on logged in user. Currently hardcoded.
    return this.getResourceWithMail('nenad.teofilovic@bsgroup.ch');
  }

  getUser(email: string){
    return this.getResourceWithMail(email);
  }

  addResource(resource: Resource){
    resource.id = this.resources.length;
    resource.imgUrl = 'http://bootdey.com/img/Content/avatar/avatar1.png';
    this.http.post<{[key: string]: string}>(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/resources.json', resource)
      .pipe(
        map(
          (response) => {
            resource.backendId = response['name'];
          }
        )
      ).subscribe(
        (response) => {
          this.resources.push(resource);
          this.resourcesChangedSubject.next(this.resources.slice());
        }
      );
  }

  deleteResource(index: number){
    this.http.delete(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/resources/' + this.resources[index].backendId + '/.json')
      .subscribe(
        (response) => {
          this.resources.splice(index, 1);
          this.resourcesChangedSubject.next(this.resources.slice());
        }
      );
  }

  editResourceWithIndex(index: number){
    this.editResourceChangedSubject.next(index);
  }

  editResource(index: number, resource: Resource){
    Object.assign( this.resources[index], resource);

    this.http.put(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/resources/' + this.resources[index].backendId + '/.json', this.resources[index])
      .subscribe(
        (response) => {
          this.resourcesChangedSubject.next(this.resources.slice());
        }
      )
  }

  editResourceWithEmail(resource: Resource){
    let resourceId = 0;
    this.resources.find(
      (res, index) => {
        if(res.email == resource.email)
          resourceId = index;
      }
    );
    this.resources[resourceId] = resource;
    this.http.put(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/resources/' + this.resources[resourceId].backendId + '/.json', this.resources[resourceId])
      .subscribe(
        (response) => {
          this.resourcesChangedSubject.next(this.resources.slice());
        }
      )
  }
}
