import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {

  isAuthenticated: boolean =  false;
  userSub: Subscription;
  authenticatedUser: User;
  isManager: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userSub = this.authService.userSubject.subscribe(
      (user: User) => {
        this.isAuthenticated = !!user;
        this.authenticatedUser = user;
        this.isManager = this.authService.isManager(this.authenticatedUser);
      }
    );
  }

  onLogout(){
    this.isAuthenticated = false;
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
