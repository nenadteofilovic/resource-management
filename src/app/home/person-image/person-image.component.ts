import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { Resource } from 'src/app/resources/model/resource.model';
import { ResourceService } from 'src/app/resources/resource.service';

@Component({
  selector: 'app-person-image',
  templateUrl: './person-image.component.html',
  styleUrls: ['./person-image.component.css']
})
export class PersonImageComponent implements OnInit, OnDestroy {
  person: Resource;
  uploadImage: boolean = false;
  userSub: Subscription;

  constructor(private resourceService: ResourceService,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.userSub = this.authService.userSubject.subscribe(
      (user: User) => {
        this.person = this.resourceService.getUser(user.email);
      }
    );

  }

  onUploadImage(){
    this.uploadImage = true;
  }

  onImageSave(elem: HTMLInputElement){
    this.person.imgUrl = elem.value;
    this.uploadImage = false;
    this.resourceService.editResourceWithEmail(this.person);
  }

  onImageCancel(){
    this.uploadImage = false;
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
