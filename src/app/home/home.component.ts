import { Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { Resource } from '../resources/model/resource.model';
import { ResourceService } from '../resources/resource.service';
import { VacationService } from '../shared/vacation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  person: Resource;
  modalRef?: BsModalRef;
  warningRef?: BsModalRef;
  vacationForm: FormGroup = new FormGroup({});
  errorMessage: string;
  userSub: Subscription;
  @ViewChild('info', { read: TemplateRef }) infoTemplate: TemplateRef<any>;

  constructor(
    private resourceService: ResourceService,
    private modalService: BsModalService,
    private vacationService: VacationService,
    private authService: AuthService) { }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.initForm();
  }

  initForm(){
    this.vacationForm.addControl('start', new FormControl(null, Validators.required));
    this.vacationForm.addControl('end', new FormControl(null, Validators.required));
    this.vacationForm.addControl('fixed-period', new FormControl(null));
  }

  ngOnInit(): void {
    this.userSub = this.authService.userSubject.subscribe(
      (user: User) => {
        this.person = this.resourceService.getUser(user.email);
      }
    )

  }

  onVacationRequestSave(){
    let start = new Date(this.vacationForm.value.start);
    let end = new Date(this.vacationForm.value.end);
    if(!this.vacationForm.value['fixed-period']){
      this.vacationForm.value['fixed-period'] = false;
    }
    let fixedPeriod = this.vacationForm.value['fixed-period'];
    try{
      this.vacationService.createRequest(this.person, start, end, fixedPeriod);
      this.clearVacationForm();
    }catch(error){
      this.errorMessage = error.message;
      this.warningRef = this.modalService.show(this.infoTemplate, {
        class: 'modal-sm',
      });
      setTimeout(() => {
        this.warningRef.hide();
      }, 3000);
    }

  }

  onVacationCancel(){
    this.clearVacationForm();
  }

  clearVacationForm(){
    this.modalRef.hide();
    this.vacationForm.reset();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
