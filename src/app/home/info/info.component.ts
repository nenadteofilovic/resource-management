import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { Resource } from 'src/app/resources/model/resource.model';
import { ResourceService } from 'src/app/resources/resource.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit, OnDestroy {
  person: Resource;
  userSub: Subscription;

  constructor(private resourceService: ResourceService,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.userSub = this.authService.userSubject.subscribe(
      (user: User) => {
        this.person = this.resourceService.getUser(user.email);
      }
    )
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
