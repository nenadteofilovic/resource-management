import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { VacationRequest } from 'src/app/resources/model/vacation-request.model';
import { ResourceService } from 'src/app/resources/resource.service';
import { VacationService } from 'src/app/shared/vacation.service';

@Component({
  selector: 'app-vacation-requests',
  templateUrl: './vacation-requests.component.html',
  styleUrls: ['./vacation-requests.component.css']
})
export class VacationRequestsComponent implements OnInit, OnDestroy {
  vacationRequests: VacationRequest[] = [];
  unsubCombinedObservable$ = new Subject();
  currentUser: User;

  constructor(private vacationService: VacationService,
              private authService: AuthService,
              private resourceService: ResourceService) {}

  ngOnInit(): void {
    combineLatest([this.vacationService.getRequests(), this.authService.userSubject])
    .pipe(
      takeUntil(this.unsubCombinedObservable$)
    ).subscribe(
      (result: [VacationRequest[], User]) => {
        this.vacationRequests = result[0];
        this.currentUser = result[1];

        if(this.authService.isUserOnHomePage()){
          this.filterVacationRequestsForHomePage();
        }
      }
    );
  }

  filterVacationRequestsForHomePage(){
    let user = this.resourceService.getUser(this.currentUser.email);
    let vacationRequestsFound: VacationRequest[] = [];
    if(user){
      vacationRequestsFound = this.vacationRequests.filter(
        request => {
          let found = request.resourceFullName === (user.firstName + ' ' + user.lastName)
          return found ? true : false;
        }
      );
    }
    this.vacationRequests = vacationRequestsFound;
  }

  ngOnDestroy(): void {
    if(this.unsubCombinedObservable$){
      // unsubscribe from returned Observable of combineLatest
      this.unsubCombinedObservable$.next();
      // kill this.unsubCombinedObservable$
      this.unsubCombinedObservable$.unsubscribe();
    }
  }

}
