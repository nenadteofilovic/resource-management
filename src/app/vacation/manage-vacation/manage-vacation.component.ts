import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { Resource } from 'src/app/resources/model/resource.model';
import { VacationRequest } from 'src/app/resources/model/vacation-request.model';
import { ResourceService } from 'src/app/resources/resource.service';
import { VacationService } from 'src/app/shared/vacation.service';

@Component({
  selector: 'app-manage-vacation',
  templateUrl: './manage-vacation.component.html',
  styleUrls: ['./manage-vacation.component.css'],
})
export class ManageVacationComponent implements OnInit, OnDestroy {
  vacationRequests: VacationRequest[] = [];
  vacationRequestsChangesSub: Subscription;
  processedVacationRequest: VacationRequest;
  modalRef: BsModalRef;
  approveVacationRequest: boolean = false;
  requestId: number;
  errorMessage: string;
  @ViewChild('info', { read: TemplateRef }) infoTemplate: TemplateRef<any>;
  vacationDaysForm = new FormGroup({});
  resources: Resource[] = [];

  constructor(
    private vacationService: VacationService,
    private resourceService: ResourceService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.vacationRequestsChangesSub =
      this.vacationService.vacationRequestsChangesSubject.subscribe(
        (requests: VacationRequest[]) => {
          this.vacationRequests = requests;
        }
      );

      this.vacationService.getRequests().subscribe(
        (requests: VacationRequest[]) => {
          this.vacationRequests = requests;
        }
      )

    this.resourceService.resourcesChangedSubject.subscribe(
      (resources: Resource[]) => {
        this.resources = resources;
      }
    );
  }

  onRejectRequest(index: number, template: TemplateRef<any>) {
    this.processedVacationRequest = this.vacationService.getRequest(index);
    this.approveVacationRequest = false;
    this.requestId = index;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  onApproveRequest(index: number, template: TemplateRef<any>) {
    this.processedVacationRequest = this.vacationService.getRequest(index);
    this.approveVacationRequest = true;
    this.requestId = index;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    this.modalRef.hide();
    try {
      if (this.approveVacationRequest) {
        this.vacationService.approveVacationRequest(this.requestId);
      } else {
        this.vacationService.rejectVacationRequest(this.requestId);
      }
    } catch (error) {
      this.errorMessage = error.message;
      this.modalRef = this.modalService.show(this.infoTemplate, {
        class: 'modal-sm',
      });
      setTimeout(() => {
        this.modalRef.hide();
      }, 2000);
    }
  }

  decline(): void {
    this.modalRef.hide();
  }

  onNewVacationDays(template: TemplateRef<any>) {
    this.initNewVacationDaysForm();
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  initNewVacationDaysForm() {
    this.vacationDaysForm.addControl(
      'employee',
      new FormControl(null, Validators.required)
    );
    this.vacationDaysForm.addControl(
      'year',
      new FormControl(null, Validators.required)
    );
    this.vacationDaysForm.addControl('days', new FormControl(null));
  }

  addNewVacationDays() {
    let employee = this.vacationDaysForm.value.employee;
    let year = this.vacationDaysForm.value.year;
    let days = this.vacationDaysForm.value.days;

    this.vacationService.addVacationDaysForEmployee(employee, year, days);
    this.clearNewVacationDaysForm();
  }

  cancelNewVacationDays() {
    this.clearNewVacationDaysForm();
  }

  clearNewVacationDaysForm() {
    this.modalRef.hide();
    this.vacationDaysForm.reset();
  }

  ngOnDestroy(): void {
    this.vacationRequestsChangesSub.unsubscribe();
  }
}
