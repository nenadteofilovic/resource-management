import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { Resource } from '../resources/model/resource.model';
import { ResourceService } from '../resources/resource.service';

@Component({
  selector: 'app-vacation',
  templateUrl: './vacation.component.html',
  styleUrls: ['./vacation.component.css']
})
export class VacationComponent implements OnInit, OnDestroy {
  person: Resource;
  userSub: Subscription;

  constructor(private resourceService: ResourceService,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.userSub = this.authService.userSubject.subscribe(
      (user: User) => {
        this.person = this.resourceService.getUser(user.email);
      }
    )
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
