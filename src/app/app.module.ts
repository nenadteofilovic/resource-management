import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ManageComponent } from './manage/manage.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectComponent } from './projects/project/project.component';
import { ProjectsComponent } from './projects/projects.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { ResourceComponent } from './resources/resource/resource.component';
import { ResourcesComponent } from './resources/resources.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { VacationComponent } from './vacation/vacation.component';
import { PersonImageComponent } from './home/person-image/person-image.component';
import { InfoComponent } from './home/info/info.component';
import { VacationRequestsComponent } from './vacation/vacation-requests/vacation-requests.component';
import { BooleanPipePipe } from './shared/boolean-pipe.pipe';
import { ManageVacationComponent } from './vacation/manage-vacation/manage-vacation.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { ManageGuard } from './manage/manage.guard';
import { AuthInterceptorService } from './auth/auth-interceptor.service';



@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    ResourcesComponent,
    ProjectsComponent,
    ProjectListComponent,
    ProjectComponent,
    ManageComponent,
    ResourceComponent,
    ResourceListComponent,
    VacationComponent,
    PersonImageComponent,
    InfoComponent,
    VacationRequestsComponent,
    BooleanPipePipe,
    ManageVacationComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [BsModalService, AuthGuard, ManageGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
