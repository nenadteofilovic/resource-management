import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { ManageComponent } from './manage/manage.component';
import { ManageGuard } from './manage/manage.guard';
import { ProjectResolver } from './projects/project-resolver.service';
import { ProjectComponent } from './projects/project/project.component';
import { ProjectsComponent } from './projects/projects.component';
import { ResourceResolverService } from './resources/resource-resolver.service';
import { ResourceComponent } from './resources/resource/resource.component';
import { ResourcesComponent } from './resources/resources.component';

const routes: Routes = [
  {path: '', redirectTo: '/auth', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard], resolve: [ResourceResolverService, ProjectResolver]},
  {path: 'manage', component: ManageComponent, canActivate: [AuthGuard, ManageGuard], resolve: [ResourceResolverService, ProjectResolver], children: [
    {
      path: "projects",
      component: ProjectComponent,
      outlet: "prj"
    },
    {
      path: "projects/:id",
      component: ProjectComponent,
      outlet: "prj"
    },
    {
      path: "resources",
      component: ResourceComponent,
      outlet: "res"
    },
    {
      path: "resources/:id",
      component: ResourceComponent,
      outlet: "res"
    }
  ]},
  {path: 'auth', component: AuthComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
