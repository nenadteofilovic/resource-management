import { HttpClient } from "@angular/common/http";
import { Injectable, resolveForwardRef } from "@angular/core";
import { Subject } from "rxjs";
import { Resource } from "../resources/model/resource.model";
import { VacationRequest } from "../resources/model/vacation-request.model";
import { Vacation } from "../resources/model/vacation.model";
import { ResourceService } from "../resources/resource.service";
import { map } from "rxjs/operators";

export enum VacationRequestStatus{
  NEW = 'New',
  IN_APPROVAL = 'In Approval',
  APPROVED = 'Approved',
  REJECTED = 'Rejected'
}

@Injectable({
  providedIn: 'root'
})
export class VacationService{
  // vacationRequests: VacationRequest[] = [
  //   new VacationRequest(0, new Date("2022-01-20"), new Date("2022-01-25"), 5, true, VacationRequestStatus.NEW)
  // ];
  vacationRequests: VacationRequest[] = [];
  vacationRequestsChangesSubject = new Subject<VacationRequest[]>();

  constructor(
    private resourceService: ResourceService,
    private http: HttpClient){}

  createRequest(person: Resource, start: Date, end: Date, fixedPeriod: boolean){
    let numberOfDays = this.getNumberOfDays(start, end);
    let request = new VacationRequest(person.id, start, end, numberOfDays, fixedPeriod, VacationRequestStatus.NEW);
    let vacationInfo = this.getVacationInfoForYear(person, start.getFullYear());
    let daysLeft = vacationInfo.daysLeft;
    if(numberOfDays > daysLeft){
      throw new Error('Vacation request is invalid. Not enough days left. Days left: ' +  daysLeft + ' Requested: ' + numberOfDays);
    }
    let fullName = this.getResourceFullName(request.resourceId);
    request.resourceFullName = fullName;

    this.http.post<{[key: string]: string}>(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/vacation-requests.json', request)
      .pipe(
        map(
          (response) => {
            request.backendId = response['name'];
          }
        )
      ).subscribe(
        (response) => {
          this.vacationRequests.push(request);
          this.vacationRequestsChangesSubject.next(this.vacationRequests.slice());
        }
      );
  }

  getRequests(){
    return this.http.get<any>('https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/vacation-requests.json')
    .pipe(
      map(
        (response) => {
          this.vacationRequests = [];
          Object.entries(response).forEach(
            (entry: any) => {
              this.vacationRequests.push({...entry[1], backendId:entry[0]});
            }
          );
          return this.vacationRequests;
        }
      )
    )
  }

  getRequest(index: number){
    return this.vacationRequests[index];
  }

  getNumberOfDays(start: Date, end: Date){
    if(start.getTime() > end.getTime()){
      throw new Error('Start date is after the end date.');
    }
    var timeDifference = end.getTime() - start.getTime();
    return timeDifference / (1000 * 3600 * 24) + 1;
  }

  rejectVacationRequest(index: number){
    this.checkStatus(this.vacationRequests[index].status);
    this.vacationRequests[index].status = VacationRequestStatus.REJECTED;
    this.http.put(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/vacation-requests/'+ this.vacationRequests[index].backendId +'/.json', this.vacationRequests[index])
      .subscribe(
        (response) => {
          this.vacationRequestsChangesSubject.next(this.vacationRequests.slice());
        }
      )
  }

  approveVacationRequest(index: number){
    this.checkStatus(this.vacationRequests[index].status);
    this.vacationRequests[index].status = VacationRequestStatus.APPROVED;

    this.http.put(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/vacation-requests/'+ this.vacationRequests[index].backendId +'/.json', this.vacationRequests[index])
      .subscribe(
        (response) => {
          this.vacationRequestsChangesSubject.next(this.vacationRequests.slice());
        }
      )
    let resource = this.resourceService.getResource(this.vacationRequests[index].resourceId);
    let vacationInfo = this.getVacationInfoForCurrentYear(resource);
    vacationInfo.daysLeft = vacationInfo.daysLeft - this.vacationRequests[index].numOfDays;
    vacationInfo.usedDays = vacationInfo.usedDays + this.vacationRequests[index].numOfDays;

    this.updateVacationInfo(resource, vacationInfo);
    this.resourceService.editResourceWithEmail(resource);

  }

  checkStatus(status: string){
    if(status !== VacationRequestStatus.NEW && status !== VacationRequestStatus.IN_APPROVAL){
      throw Error('Status of the vacation request can not be changed.');
    }
  }

  getVacationInfoForCurrentYear(resource: Resource){
    return this.getVacationInfoForYear(resource, (new Date()).getFullYear())
  }

  getVacationInfoForYear(resource: Resource, year: number){
    let vacations: Vacation[] = resource.vacation;
    let vacationInfo = vacations.find(
      vacation =>
       { return vacation.year === year}
    );
    return vacationInfo;
  }

  updateVacationInfo(resource: Resource, vacationInfo: Vacation){
    let infoForYear = resource.vacation.find(
      vacation =>
       vacation.year === (new Date()).getFullYear()
    );
    infoForYear = vacationInfo;
  }

  addVacationDaysForEmployee(email: string, year: number, days: number){
    let resource: Resource = this.resourceService.getResourceWithMail(email);
    if(!resource.vacation){
      resource.vacation = [];
    }
    resource.vacation.push(new Vacation(year, days, 0, days));
    this.resourceService.editResource(resource.id, resource);
  }

  getResourceFullName(resourceId: number){
    let resource = this.resourceService.getResource(resourceId);
    return resource.firstName + ' ' +  resource.lastName;
  }

}
