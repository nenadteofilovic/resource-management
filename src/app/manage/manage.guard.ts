import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth.service";
import { map } from 'rxjs/operators';
import { User } from "../auth/user.model";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ManageGuard implements CanActivate{

  constructor(private authService: AuthService,
              private router: Router){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.authService.userSubject.pipe(
      map(
        (user: User) => {
          const isAuth = !!user;
          return (isAuth && this.authService.isManager(user)) ? true : this.router.createUrlTree(['/home']);
        }
      )
    );
  }

}
