export enum ProjectStatus{
  New = 'New',
  Active = 'Active',
  Finished = 'Finished'
}

export class Project {
  constructor(
    public id: number,
    public name: string,
    public client: string,
    public startDate: Date,
    public endDate: Date,
    public projectMembers: [string, number][],
    public status: ProjectStatus,
    public backendId?: string
  ) {}
}
