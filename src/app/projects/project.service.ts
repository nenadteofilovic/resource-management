import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { Project, ProjectStatus } from "./project.model";
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectService{
  // projects: Project[] = [
  //   new Project('Angular Project', 'ZKB', new Date("2019-01-16"), new Date("2022-05-15"), [
  //     ['Andreas Graf', 2]
  //   ], ProjectStatus.Active ),
  //   new Project('REST Project', 'Die Post', new Date("2021-12-16"), new Date("2022-09-30"), [
  //     ['Pascal Langenstein', 1],
  //     ['Nenad Teofilovic', 0]
  //   ], ProjectStatus.New )
  // ];
  projects: Project[] = [];
  projectsChangedSubject = new BehaviorSubject<Project[]>([]);
  editProjectChangedSubject = new Subject<number>();

  constructor(private http: HttpClient){}

  getProjects(){
    return this.http.get<any>('https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/projects.json')
    .pipe(
      tap(
        (response) => {
          this.projects = [];
          if(response){
            Object.entries(response).forEach(
              (entry: any) => {
                this.projects.push({...entry[1], backendId:entry[0], endDate: new Date(entry[1].endDate), startDate: new Date(entry[1].startDate)});
              }
            );
          }
          this.projectsChangedSubject.next(this.projects.slice());
        }
      )
    )
  }

  getProject(index: number){
    return this.projects[index];
  }

  addProject(project: Project){
    project.id = this.projects.length;
    this.http.post<{[key: string]: string}>(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/projects.json', project)
      .pipe(
        map(
          (response) => {
            project.backendId = response['name'];
          }
        )
      ).subscribe(
        (response) => {
          this.projects.push(project);
          this.projectsChangedSubject.next(this.projects.slice());
        }
      );
  }

  deleteProject(index: number){
    this.http.delete(
      'https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/projects/' + this.projects[index].backendId + '/.json')
      .subscribe(
        (response) => {
          this.projects.splice(index, 1);
          this.projectsChangedSubject.next(this.projects.slice());
        }
      );

  }

  editProjectWithIndex(index:number){
    //Use this Observable to notify component when secondary route is changed
    this.editProjectChangedSubject.next(index);
  }

  editProject(index: number, project: Project){
    project.startDate = new Date(project.startDate);
    project.endDate = new Date(project.endDate);
    project.backendId = this.projects[index].backendId;
    Object.assign( this.projects[index], project);
    this.http.put('https://ng-resource-management-default-rtdb.europe-west1.firebasedatabase.app/projects/' + this.projects[index].backendId + '/.json', this.projects[index])
    .subscribe(
      (response) => {
        this.projectsChangedSubject.next(this.projects.slice());
      }
    );

  }
}
