import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { Project } from "./project.model";
import { ProjectService } from "./project.service";

@Injectable({
  providedIn: 'root'
})
export class ProjectResolver implements Resolve<Project[]>
{
  constructor(private projectService: ProjectService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Project[] | Observable<Project[]> | Promise<Project[]> {
    return this.projectService.getProjects();
  }

}
