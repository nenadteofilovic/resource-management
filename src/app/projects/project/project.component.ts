import { Component, OnChanges, OnDestroy, OnInit, resolveForwardRef, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { Resource } from 'src/app/resources/model/resource.model';
import { ResourceService } from 'src/app/resources/resource.service';
import { ProjectListComponent } from '../project-list/project-list.component';
import { Project, ProjectStatus } from '../project.model';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, OnDestroy {

  projectForm: FormGroup = new FormGroup({});
  editMode: boolean = false;
  projectId: number;
  editProjectChangedSub: Subscription;
  resourcesChangedSub: Subscription;
  @ViewChild('editConfirm', { read: TemplateRef }) editConfirmTemplate:TemplateRef<any>;
  modalRef: BsModalRef;
  resources: Resource[];
  projectStatuses =  ProjectStatus;
  projectStatusKeys: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private modalService: BsModalService,
    private resourceService: ResourceService) {
    }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.projectId = +params['id'];
        this.editMode = params['id'] != null;
      }
    );
    //Subscribe to this Observable and catch event when secondary route is changed
    this.editProjectChangedSub = this.projectService.editProjectChangedSubject.subscribe(
      (index: number) => {
        //Workaround: Fetch the new index of the project (from secondary route), reload the route and navigate again to new secondary route.
        //This will trigger, reload of the component.
        this.projectId = index;
        this.navigateAfterFormIsClosed();
        setTimeout(() => {
          let newUrl = '';
          if(this.router.url.indexOf('res:resources') != -1){
            newUrl = this.router.url.replace('//', '');
            newUrl = this.router.url.replace(')','//prj:projects/'+ this.projectId +')');
          }else{
            newUrl = '/manage/(prj:projects/'+index+')';
          }
          this.router.navigateByUrl(newUrl);
        },100);

      }
    );

    this.resourcesChangedSub = this.resourceService.resourcesChangedSubject.subscribe(
      (resources: Resource[]) => {
        this.resources = resources;
      }
    )
    this.initForm();

    this.projectStatusKeys = Object.keys(this.projectStatuses);

  }

  initForm(){

    let name = null;
    let client = null;
    let startDate = null;
    let endDate = null;
    let members = [];
    let status = ProjectStatus.New


    if(this.editMode){
      let project = this.projectService.getProject(this.projectId);
      name = project.name;
      client = project.client;
      startDate = project.startDate.toISOString().substring(0, 10);
      endDate = project.endDate.toISOString().substring(0, 10);
      members = project.projectMembers;
      status = project.status;
    }

    this.projectForm.addControl('name', new FormControl(name, Validators.required));
    this.projectForm.addControl('client', new FormControl(client, Validators.required));
    this.projectForm.addControl('startDate', new FormControl(startDate, Validators.required));
    this.projectForm.addControl('endDate', new FormControl(endDate, Validators.required));
    this.projectForm.addControl('status', new FormControl(status, Validators.required));
    this.projectForm.addControl('projectMembers', new FormArray([]));

    if(this.editMode){
      let projectMembersArray = this.projectForm.get('projectMembers') as FormArray;
      members.forEach(
        member => {
          projectMembersArray.push(
            new FormGroup({
              'resourceList': new FormControl(this.formatMemberForDropDownElem(member), Validators.required)
            })
          )
        }
      )
    }

  }

  formatMemberForDropDownElem(member: [string, number]){
    return member[0].trim() + '-' + member[1];
  }

  onSave(){
    let membersArray: [string, number][] = [];
    let members = (this.projectForm.get('projectMembers') as FormArray).controls;
    members.forEach(element => {
      let resourceInfo = element.value.resourceList.split('-', 2);
      membersArray.push([resourceInfo[0].trim(), +resourceInfo[1].trim()]);
    });
    let project = new Project(
      this.projectId,
      this.projectForm.controls['name'].value,
      this.projectForm.controls['client']?.value,
      new Date(this.projectForm.get('startDate')?.value),
      new Date(this.projectForm.controls['endDate']?.value),
      membersArray,
      this.projectForm.controls['status'].value);

    if(this.editMode){
      this.projectService.editProject(this.projectId, project);
    }else{
      this.projectService.addProject(project);
    }
    this.navigateAfterFormIsClosed();

    this.modalRef = this.modalService.show(this.editConfirmTemplate, {class: 'modal-sm'});
    setTimeout(() => {
      this.modalRef.hide();
    },2000);
  }

  onCancel(){
    this.navigateAfterFormIsClosed();
  }

  navigateAfterFormIsClosed(){
    let newUrl = '';
    if(this.router.url.indexOf('res:resources') != -1){
      let delimiterIndex = this.router.url.indexOf('//');
      newUrl = this.router.url.substring(0, delimiterIndex);
      newUrl = newUrl + ')';
    }else{
      newUrl = '/manage';
    }
    this.router.navigateByUrl(newUrl);
  }

  get projectMembers(){
    return (<FormArray>this.projectForm.get('projectMembers')).controls;
  }

  onAddMember(){
    (<FormArray>this.projectForm.get('projectMembers')).push(new FormGroup({
      'resourceList': new FormControl(null)
    }))
  }

  onDeleteMember(index: number){
    (<FormArray>this.projectForm.get('projectMembers')).removeAt(index);
  }

  ngOnDestroy(): void {
    this.editProjectChangedSub.unsubscribe();
    this.resourcesChangedSub.unsubscribe();
  }



}
