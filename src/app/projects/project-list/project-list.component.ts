import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { ResourceService } from 'src/app/resources/resource.service';
import { Project } from '../project.model';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  projects: Project[] = [];
  modalRef: BsModalRef;
  projectToDelete: Project;
  projectIndexToDelete: number;
  displayButtons: boolean = false;
  currentUser: User;
  unsubCombinedObservable$ = new Subject();

  constructor(private projectService: ProjectService,
              private router: Router,
              private modalService: BsModalService,
              private resourceService: ResourceService,
              private authService: AuthService) { }


  ngOnInit(): void {

    combineLatest([this.projectService.projectsChangedSubject, this.authService.userSubject])
    .pipe(
      takeUntil(this.unsubCombinedObservable$)
    ).subscribe(
      (result: [Project[], User]) => {
      this.projects = result[0];
      this.currentUser = result[1];

      if(this.authService.isUserOnHomePage()){
        this.filterProjectsForHomePage();
      }
    });

   if(!this.authService.isUserOnHomePage()){
     this.displayButtons = true;
   }
  }

  filterProjectsForHomePage(){
    let user = this.resourceService.getUser(this.currentUser.email);
    let projectsFound: Project[] = [];
    if(user){
      projectsFound = this.projects.filter(
        project => {
          const members: [string, number][] = project.projectMembers;
          let found = members.find(
            member => member[0] == (user.firstName + ' ' + user.lastName)
          );
          return found ? true : false;
        }
      );
    }
    this.projects = projectsFound;
  }

  onNewProject(){
    let newUrl = '';
    if(this.router.url.indexOf('res:resources') != -1){
      let delimiterIndex = this.router.url.indexOf('//');
      if(delimiterIndex == -1){
        newUrl = this.router.url.replace(')', '//prj:projects)');
      }else{
        newUrl = this.router.url.substring(0, delimiterIndex);
        newUrl = newUrl + '//prj:projects)';
      }
    }else{
      newUrl = '/manage/(prj:projects)';
    }
    this.router.navigateByUrl(newUrl);
  }

  onDelete(index: number, template: TemplateRef<any>){
   this.projectToDelete = this.projects[index];
   this.projectIndexToDelete = index;
   this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  confirm(): void {
    this.modalRef.hide();
    this.projectService.deleteProject(this.projectIndexToDelete);
  }

  decline(): void {
    this.modalRef.hide();
  }

  onEdit(index: number){
    //Since secondary route change does not trigger component reload, we have to notify component using Subject in the service
    let newUrl = '';
    if(this.router.url.indexOf('res:resources') != -1){
      newUrl = this.router.url.replace('//','');
      newUrl = newUrl.replace(')','//prj:projects/'+index+')');
    }else{
      newUrl = '/manage/(prj:projects/'+ index +')';
    }
    this.router.navigateByUrl(newUrl);
    this.projectService.editProjectWithIndex(index);
  }

  ngOnDestroy(): void {
    if(this.unsubCombinedObservable$){
      // unsubscribe from returned Observable of combineLatest
      this.unsubCombinedObservable$.next();
      // kill this.unsubCombinedObservable$
      this.unsubCombinedObservable$.unsubscribe();
    }
  }

}
