import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { User } from "./user.model";

export interface AuthResponseData{
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  userSubject = new BehaviorSubject<User>(null);

  constructor(private httpClient: HttpClient,
              private router: Router){}

  login(email: string, password: string){
    return this.httpClient.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCVswXxqFTYAy0YhFx8dF42Fmc_deycvdc',
    {
      email: email,
      password: password,
      returnSecureToken: true
    })
    .pipe(
      catchError(this.handleError),
      tap(
        (response) => {
          this.handleAuthentication(response);
        }
      )
    )
    }

  logout(){
    this.userSubject.next(null);
    this.router.navigate(['/auth']);
  }

  signup(email: string, password: string){
    return this.httpClient.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCVswXxqFTYAy0YhFx8dF42Fmc_deycvdc',
    {
      email: email,
      password: password,
      returnSecureToken: true
    })
    .pipe(
      catchError(this.handleError),
      tap(
        (response) => {
          this.handleAuthentication(response);
        }
      )
    )
  }

  handleAuthentication(authData: AuthResponseData){
    const expiresIn: number = +authData.expiresIn * 1000;
    const user = new User(authData.email, authData.localId, authData.idToken, new Date(new Date().getTime() + expiresIn));
    this.userSubject.next(user);
  }

  handleError(errorResponse: HttpErrorResponse){
    let errorMessage = 'An unknown error occurred!';
    if(errorResponse.error && errorResponse.error.error){
        switch(errorResponse.error.error.message){
            case 'EMAIL_EXISTS':
                errorMessage = 'This email already exists';
                break;
            case 'EMAIL_NOT_FOUND':
                errorMessage = 'This email does not exist';
                break;
            case 'INVALID_PASSWORD':
                errorMessage = 'This password is not correct';
                break;
        }
    }
    return throwError(errorMessage);
  }

  isUserOnHomePage(){
    return this.router.url === '/home';
  }

  isManager(user: User){
    return !!user && user.email === 'management@bsgroup.ch';
  }

}
