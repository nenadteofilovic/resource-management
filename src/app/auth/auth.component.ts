import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponseData, AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  userExist: boolean = false;
  showAuthForm: boolean = false;
  errorMessage: string;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){

    const email = form.value.email;
    const password = form.value.password;

    let auth$: Observable<AuthResponseData> = null;
    if(this.userExist){
      auth$ = this.authService.login(email, password);
    }else{
      const rePassword = form.value.repassword;
      if(password !== rePassword){
        this.errorMessage = 'Entered values for the password are not equal. Please try again.'
      }else{
        auth$ = this.authService.signup(email, password);
      }

    }

    auth$.subscribe(
      (authResponse) => {
        this.router.navigate(['/home']);
      },
      (errorMessage) => {
        this.errorMessage = errorMessage;
      }
    );

    form.reset();

  }

  onLoginSelect(){
    this.userExist = true;
    this.showAuthForm = true;
  }

  onSignupSelect(){
    this.userExist = false;
    this.showAuthForm = true;
  }

  onCancel(){
    this.showAuthForm = false;
    this.userExist = false;
    this.errorMessage = null;
  }
}
